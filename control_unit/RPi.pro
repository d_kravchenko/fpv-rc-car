#-------------------------------------------------
#
# Project created by QtCreator 2016-02-05T00:09:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RPi
TEMPLATE = app

#LIBS += -L/usr/local/lib -lbcm2835
#LIBS += -L/usr/lib/arm-linux-gnueabihf -lrt
#INCLUDEPATH += /usr/local/include


SOURCES += main.cpp\
        mainwindow.cpp \
    adcserial.cpp \
    antenna.cpp \
    calibratedialog.cpp \
    settingsdialog.cpp

HEADERS  += mainwindow.h \
    adcserial.h \
    antenna.h \
    calibratedialog.h \
    player.h \
    settingsdialog.h

FORMS    += mainwindow.ui

