#include "settingsdialog.h"
#include <QLabel>
#include <QLayout>
#include <QTimer>
#include <QGroupBox>


SettingsDialog::SettingsDialog(QWidget* parent)
    : QDialog(parent)
{
    setModal(false);

    QVBoxLayout* groupBoxLayout = new QVBoxLayout;

    labelsList_.append(new QLabel("State :"));
    labelsList_.append(new QLabel("Bind :"));
    labelsList_.append(new QLabel("Speed Mode :"));
    labelsList_.append(new QLabel("Turn Wheel "));
    labelsList_.append(new QLabel("Max Val :"));
    labelsList_.append(new QLabel("Mid Val :"));
    labelsList_.append(new QLabel("Min Val :"));
    labelsList_.append(new QLabel("Accelerator "));
    labelsList_.append(new QLabel("Max Val :"));
    labelsList_.append(new QLabel("Mid Val :"));
    labelsList_.append(new QLabel("Min Val :"));

    this->setStyleSheet("QLabel { font-family: Rockwell; font-weight: bold; font-size: 13px}"
                        "QGroupBox { font-family: Pristina; font-weight: bold; font-size: 25px}");

    for (int i = 0; i < labelsList_.size(); i++)
        groupBoxLayout->addWidget(labelsList_.at(i));


    groupBox_ = new QGroupBox;
    groupBox_->setGeometry(200,10,220,420);
    groupBox_->setTitle(QString("Car 1"));
    groupBox_->setLayout(groupBoxLayout);

    QHBoxLayout* mainLayout = new QHBoxLayout;
    mainLayout->addWidget(groupBox_);

    setLayout(mainLayout);

    setWindowTitle("Settings");
    setFixedSize(groupBox_->size());

    refresh();

}

void SettingsDialog::refresh()
{
        labelsList_[0]->setText(labelsList_[0]->text() + "Ready to Race");

        labelsList_[1]->setText(labelsList_[1]->text() + "Ready");

        labelsList_[2]->setText(labelsList_[2]->text() + "Normal");

}
