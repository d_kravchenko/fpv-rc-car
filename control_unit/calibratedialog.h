#ifndef CALIBRATEDIALOG_H
#define CALIBRATEDIALOG_H

#include <QDialog>
#include "player.h"
#include "adcserial.h"

class QLineEdit;
class QComboBox;
class QTimer;

class CalibrateDialog : public QDialog
{
    Q_OBJECT
private:
    QLineEdit* maxValueTxt_;
    QLineEdit* midValueTxt_;
    QLineEdit* minValueTxt_;
    QComboBox* potentiomBox_;
    QTimer* timer_;
    struct player* p_;
    AdcSerial& adc_;
    int currentChannel_;
    int currentIndex_;
    volatile int val_;

public:
    explicit CalibrateDialog(int channel, struct player* p, const AdcSerial& adc, QWidget* parent = 0);

signals:
    void clicked(int id);

private slots:
    void onCurrentIndexChanged(const QString &text);
    void refreshTxt();
    void setButtonClicked(int id);
};

#endif // CALIBRATEDIALOG_H
