#include "adcserial.h"


AdcSerial::AdcSerial(QObject* parent)
    : QObject(parent)
{
    std::memset(rbuff_, 0, sizeof rbuff_);
    std::memset(tbuff_, 0, sizeof tbuff_);

    this->initSerial();
}

AdcSerial::~AdcSerial()
{
    bcm2835_spi_end();
    bcm2835_close();
}

int AdcSerial::initSerial(uint8_t bitOrder, uint8_t mode, uint16_t divider, uint8_t chipSelect, uint8_t chipActive)
{
    // Use for testing
    //        bcm2835_set_debug(1);
    if (!bcm2835_init())
    {
        perror("SPI bcm error\n");
        return -1;
    }
    if (chipActive == 0)
    {
        chipActive = LOW;
    }
    else
    {
        chipActive = HIGH;
    }
    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(bitOrder);                  // The default MSB first
    bcm2835_spi_setDataMode(mode);                      // The default MODE_0
    bcm2835_spi_setClockDivider(divider);               // 600MHz / 512 = 1.1718MHz
    bcm2835_spi_chipSelect(chipSelect);                 // The default CS0
    bcm2835_spi_setChipSelectPolarity(chipSelect, chipActive);      // The default LOW

    return 0;
}

int AdcSerial::getData(unsigned int adcNum)
{
//    QByteArray arr;
//    arr.resize(3);
//    arr[0] = 0x01;
    tbuff_[0] = 0x01;
    tbuff_[1] = 0x80 | ((adcNum & 0x07) << 4);
    tbuff_[2] = 0x00;
    int value = 0;
    bcm2835_spi_transfernb(tbuff_, rbuff_, sizeof(rbuff_));
    value = ((rbuff_[1] & 0x03) << 8);
    value |= (rbuff_[2] & 0xff);
//    bcm2835_delay(500);
    return value;
}
