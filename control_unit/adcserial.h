#ifndef ADCSERIAL_H
#define ADCSERIAL_H

#include <QObject>
#include <bcm2835.h>
#include <cstring>
#include <cstdio>

class AdcSerial : public QObject
{
    Q_OBJECT
public:
    explicit AdcSerial(QObject *parent = 0);
    ~AdcSerial();

    int initSerial(uint8_t bitOrder = 1, uint8_t mode = 0, uint16_t divider = 512, uint8_t chipSelect = 0, uint8_t chipActive = 0);
    void startRefreshTimer(int msec, unsigned int adcNum = 0);
    int getData(unsigned int adcNum);

protected:
    char tbuff_[3];
    char rbuff_[3];


};

#endif // ADCSERIAL_H
