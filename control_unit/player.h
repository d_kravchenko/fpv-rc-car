#ifndef PLAYER_H
#define PLAYER_H

struct controller
{
  int nullMin, nullMax, Middle, Min, Max;
};

struct player
{
    int ch;
    bool speedMode, plugged, bind, enable;
    controller accelerator, wheel;
};

#endif // PLAYER_H
