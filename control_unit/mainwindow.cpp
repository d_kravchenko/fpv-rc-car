#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->bindButton, SIGNAL(clicked()), this, SLOT(bindChannel()));
    connect(ui->calibrateButton, SIGNAL(clicked()), this, SLOT(calibrateController()));
    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(startRace()));
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(stopRace()));
    this->setFixedSize(402, 435);

    adc_ = new AdcSerial();
    uart_ = new Antenna();

    QSignalMapper* signalMapper = new QSignalMapper(this);

    connect(ui->mode_ch1_sw, SIGNAL(valueChanged(int)), signalMapper, SLOT(map()));
    connect(ui->mode_ch2_sw, SIGNAL(valueChanged(int)), signalMapper, SLOT(map()));
    connect(ui->mode_ch3_sw, SIGNAL(valueChanged(int)), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->mode_ch1_sw, 0);
    signalMapper->setMapping(ui->mode_ch2_sw, 1);
    signalMapper->setMapping(ui->mode_ch3_sw, 2);

    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(chMode(int)));

    std::memset(pl_, 0, sizeof pl_);
    ui->ch1Label->setStyleSheet(QString("QLabel { background-color: #FF0505; }"));
    ui->ch2Label->setStyleSheet(QString("QLabel { background-color: #FF0505; }"));
    ui->ch3Label->setStyleSheet(QString("QLabel { background-color: #FF0505; }"));
}

MainWindow::~MainWindow()
{
    delete adc_;
    delete uart_;
    delete ui;
}

bool MainWindow::isReady(int channel)
{
    qDebug() <<QString("Plugged ")<<pl_[channel].plugged<<endl;
    qDebug() <<QString("Binded ")<<pl_[channel].bind<<endl;

    if (pl_[channel].plugged && pl_[channel].bind)
    {
        switch (channel)
        {
            case 0:
                ui->ch1Label->setStyleSheet(QString("QLabel { background-color: #39D82A; }"));
                break;
            case 1:
                ui->ch2Label->setStyleSheet(QString("QLabel { background-color: #39D82A; }"));
                break;
            case 2:
                ui->ch3Label->setStyleSheet(QString("QLabel { background-color: #39D82A; }"));
                break;
        }
        pl_[channel].enable = true;
        return true;
    }
    else if (pl_[channel].plugged || pl_[channel].bind)
    {
        switch (channel)
        {
            case 0:
                ui->ch1Label->setStyleSheet(QString("QLabel { background-color: #FFC646; }"));
                break;
            case 1:
                ui->ch2Label->setStyleSheet(QString("QLabel { background-color: #FFC646; }"));
                break;
            case 2:
                ui->ch3Label->setStyleSheet(QString("QLabel { background-color: #FFC646; }"));
                break;
        }
        return false;
    }

    return false;
}

void MainWindow::calibrateController()
{
    int channel = ui->gameChannelDial->value();
    qDebug() <<QString("Channel :")<<channel<<endl;
//    if (!pl_[channel].plugged)
//    {
        CalibrateDialog* dialog = new CalibrateDialog(channel, &pl_[channel], adc_);
        if (dialog->exec() == QDialog::Accepted)
        {
            pl_[channel].accelerator.nullMax = pl_[channel].accelerator.Middle + 20;
            pl_[channel].accelerator.nullMin = pl_[channel].accelerator.Middle - 20;


            pl_[channel].wheel.nullMax = pl_[channel].wheel.Middle + 20;
            pl_[channel].wheel.nullMin = pl_[channel].wheel.Middle - 20;

            // Receive middle, max and min data from potentiometr
            qDebug() <<pl_[channel].accelerator.Max<<endl;
            qDebug() <<pl_[channel].accelerator.Middle<<endl;
            qDebug() <<pl_[channel].accelerator.Min<<endl;

            qDebug() <<pl_[channel].wheel.Max<<endl;
            qDebug() <<pl_[channel].wheel.Middle<<endl;
            qDebug() <<pl_[channel].wheel.Min<<endl;

            pl_[channel].plugged = true;
        }
        delete dialog;
//    }
    this->isReady(channel);
}

void MainWindow::raceThread()
{
    forever
    {
//        QApplication::processEvents();
        char send[15];
        for (int i = 0; i < 3; i++)
        {
            if (pl_[i].enable)
            {
                bcm2835_delay(250);
                mutex_.lock();
                int accelerator = adc_->getData((i * 2) + 1);
                int wheel = adc_->getData(i * 2);
                accelerator = normalize(accelerator, pl_[i].accelerator);
                if (accelerator < 50)
                    wheel += 30;
                wheel = normalize(wheel, pl_[i].wheel);
                mutex_.unlock();
                std::snprintf(send, 15, "#C%dR%d/%d?", i, accelerator, wheel);
                qDebug() <<send<<endl;
                mutex_.lock();
                uart_->sendData(send, sizeof send);
                mutex_.unlock();
            }
        }
        QMutexLocker locker(&mutex_);
        if (!raceStarted_)
            break;
    }
}

int MainWindow::normalize(int value, struct controller& c)
{
    int res = 0;
    if ((c.nullMin <= value) && (value <= c.nullMax))
    {
        res = 0;
    }
    else
    {
        if (value < c.nullMin)
        {
            res = (((value - c.nullMin) * 100) / (c.nullMin - c.Min));
        }
        else
            res = (((int)(value - c.nullMax) * 100) / (c.Max - c.nullMax));
    }

    res += 100;
    if (res < 0)
        res = 0;
    else if (res > 200)
        res = 200;

    return res;
}

void MainWindow::startRace()
{
    if (!raceStarted_)
    {
        bool permitted = pl_[0].enable || pl_[1].enable || pl_[2].enable;
        if (permitted)
        {
            ui->speedModeGroup->setDisabled(true);
            ui->selectChannelGroup->setDisabled(true);
            uart_->sendData("#Start?", 8);
            raceStarted_ = true;
            QFuture<void> future = QtConcurrent::run(this, &MainWindow::raceThread);
            qDebug() <<QString("Race started")<<endl;
        }
        else
        {
            qDebug() <<QString("Cannot start race without any players ")<<endl;
        }
    }
}

void MainWindow::stopRace()
{
    char send[] = "#Stop?";
    ui->speedModeGroup->setEnabled(true);
    ui->selectChannelGroup->setEnabled(true);
    QMutexLocker locker(&mutex_);
    raceStarted_ = false;
    qDebug() <<QString("Race stopped")<<endl;
    uart_->sendData(send, sizeof send);     // send Stop command to all cars
}

void MainWindow::chMode(int index)
{
    qDebug() <<QString("Index :")<<index<<endl;
    if (index == 0)
    {
        pl_[0].speedMode = (bool)ui->mode_ch1_sw->value();
    }
    else if (index == 1)
    {
        pl_[1].speedMode = (bool)ui->mode_ch2_sw->value();
    }
    else
    {
        pl_[2].speedMode = (bool)ui->mode_ch3_sw->value();
    }
}

void MainWindow::bindChannel()
{
    qint8 channel = ui->gameChannelDial->value();
    qDebug() <<QString("Channel :")<<channel<<endl;
//    if (!pl_[channel].bind)
//    {
        char send[8], rec[8];
        std::memset(send, 0, sizeof send);
        std::memset(rec, 0, sizeof rec);
        std::snprintf(send, 8, "#B%d?", channel);

        uart_->sendData(send, sizeof send);
        qDebug() <<send<<sizeof send<<endl;

        uart_->recData(rec, sizeof rec);
        qDebug() <<rec<<sizeof rec<<endl;

        if (std::strstr(rec, "Bind+") != 0)
        {
            // send data to car and receive( car channel id e.g. 2, )
            pl_[channel].bind = true;
            qDebug() <<QString("Bind is OK")<<endl;
        }
        else
        {
            qDebug() <<QString("Cannot bind")<<endl;
            pl_[channel].bind = false;
        }
//    }
    this->isReady(channel);
}
