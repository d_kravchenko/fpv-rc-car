#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QList>
#include <QDialog>

class QLabel;
class QGroupBox;
class QTimer;
class QHBoxLayout;

class SettingsDialog : public QDialog
{
private:
    QList<QLabel*> labelsList_;
    QGroupBox* groupBox_;
    QTimer* timer_;

public:
    SettingsDialog(QWidget *parent = 0);

public slots:
    void refresh();
};

#endif // SETTINGSDIALOG_H
