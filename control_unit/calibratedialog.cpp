#include "calibratedialog.h"
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QLayout>
#include <QTimer>
#include <QSignalMapper>
//#include <QtGui>

#include <qdebug.h>

CalibrateDialog::CalibrateDialog(int channel, player* p, const AdcSerial& adc, QWidget* parent)
    : QDialog(parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    setModal(true);

    val_ = 0;
    p_ = p;
    adc_ = adc;
    currentChannel_ = channel;

    maxValueTxt_ = new QLineEdit;
    midValueTxt_ = new QLineEdit;
    minValueTxt_ = new QLineEdit;

    maxValueTxt_->setDisabled(true);
    midValueTxt_->setDisabled(true);
    minValueTxt_->setDisabled(true);

    potentiomBox_ = new QComboBox;

    potentiomBox_->addItem("Accelerator");
    potentiomBox_->addItem("Turn wheel");

    connect(potentiomBox_, SIGNAL(currentIndexChanged(QString)), this, SLOT(onCurrentIndexChanged(QString)));

    QLabel* potentiomLabel = new QLabel("Choose potentiometr :");
    QLabel* maxLabel = new QLabel("Max. Value");
    QLabel* midLabel = new QLabel("Mid. Value");
    QLabel* minLabel = new QLabel("Min. Value");

    potentiomLabel->setBuddy(potentiomBox_);

    QPushButton* maxButton = new QPushButton("Set Max.");
    QPushButton* midButton = new QPushButton("Set Mid.");
    QPushButton* minButton = new QPushButton("Set Min.");
    QSignalMapper* signalMapper = new QSignalMapper(this);

    connect(maxButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(midButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(minButton, SIGNAL(clicked()), signalMapper, SLOT(map()));

    signalMapper->setMapping(maxButton, 0);
    signalMapper->setMapping(midButton, 1);
    signalMapper->setMapping(minButton, 2);

    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(clicked(int)));
    connect(this, SIGNAL(clicked(int)), this, SLOT(setButtonClicked(int)));

    QPushButton* okButton = new QPushButton("OK");
    QPushButton* cancelButton = new QPushButton("Cancel");

    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    QHBoxLayout* topLayout = new QHBoxLayout;
    topLayout->addWidget(potentiomLabel);
    topLayout->addWidget(potentiomBox_);

    QGridLayout* middleLayout = new QGridLayout;
    middleLayout->addWidget(maxLabel, 0, 0);
    middleLayout->addWidget(maxValueTxt_, 0, 1);
    middleLayout->addWidget(maxButton, 0, 2);

    middleLayout->addWidget(midLabel, 1, 0);
    middleLayout->addWidget(midValueTxt_, 1, 1);
    middleLayout->addWidget(midButton, 1, 2);

    middleLayout->addWidget(minLabel, 2, 0);
    middleLayout->addWidget(minValueTxt_, 2, 1);
    middleLayout->addWidget(minButton, 2, 2);

    QHBoxLayout* bottomLayout = new QHBoxLayout;
    bottomLayout->addStretch();
    bottomLayout->addWidget(okButton);
    bottomLayout->addWidget(cancelButton);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(middleLayout);
    mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);

    setWindowTitle("Calibration");
    setFixedSize(sizeHint());

    timer_ = new QTimer;

    connect(timer_, SIGNAL(timeout()), this, SLOT(refreshTxt()));
    timer_->start(250);

    potentiomBox_->currentIndexChanged("Accelerator");
}

void CalibrateDialog::onCurrentIndexChanged(const QString &text)
{
    if (text == "Accelerator")
    {
        qDebug() <<QString("Accelerator")<<endl;
        currentIndex_ = 0;
//        emit refreshTxt();
    }
    else
    {
        qDebug() <<QString("Turn wheel")<<endl;
        currentIndex_ = 1;
//        emit refreshTxt();
    }
}

void CalibrateDialog::refreshTxt()
{
    if (currentIndex_ == 0)
    {
        val_ = adc_->getData((currentChannel_ * 2) + 1);
        if (!p_->accelerator.Max)
            maxValueTxt_->setText(QString::number(val_));
        if (!p_->accelerator.Middle)
            midValueTxt_->setText(QString::number(val_));
        if (!p_->accelerator.Min)
            minValueTxt_->setText(QString::number(val_));
    }
    else
    {
        val_ = adc_->getData(currentChannel_ * 2);
        if (!p_->wheel.Max)
            maxValueTxt_->setText(QString::number(val_));
        if (!p_->wheel.Middle)
            midValueTxt_->setText(QString::number(val_));
        if (!p_->wheel.Min)
            minValueTxt_->setText(QString::number(val_));
    }
}

void CalibrateDialog::setButtonClicked(int id)
{
    if (currentIndex_ == 0)
    {
        switch (id)
        {
            case 0:
                p_->accelerator.Max = val_;
                maxValueTxt_->setText(QString::number(val_));
                break;
            case 1:
                p_->accelerator.Middle = val_;
                midValueTxt_->setText(QString::number(val_));
                break;
            case 2:
                p_->accelerator.Min = val_;
                minValueTxt_->setText(QString::number(val_));
                break;
        }
    }
    else
    {
        switch (id)
        {
            case 0:
                p_->wheel.Max = val_;
                maxValueTxt_->setText(QString::number(val_));
                break;
            case 1:
                p_->wheel.Middle = val_;
                midValueTxt_->setText(QString::number(val_));
                break;
            case 2:
                p_->wheel.Min = val_;
                minValueTxt_->setText(QString::number(val_));
                break;
        }
    }
}
