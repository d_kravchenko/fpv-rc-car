#include "antenna.h"

Antenna::Antenna()
{
    fd_ = -1;
    std::memset(&serial_, 0, sizeof serial_);
    std::memset(&old_serial_, 0, sizeof old_serial_);

    this->initUart();
}

Antenna::~Antenna()
{
    tcsetattr(fd_, TCSADRAIN, &old_serial_); // Apply old configuration
    close(fd_);
}

int Antenna::initUart()
{
    fd_ = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY);

    if (fd_ == -1) {
        perror("Error - Unable to open UART.  Ensure it is not in use by another application\n");
        return -1;
    }

    if (tcgetattr(fd_, &old_serial_) < 0) {
        perror("Getting configuration");
        return -1;
    }

    // Set up Serial Configuration

//    memcpy(&serial, &old_serial, sizeof(old_serial));

    /*
      BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
      CRTSCTS : output hardware flow control (only used if the cable has
                all necessary lines. See sect. 7 of Serial-HOWTO)
      CS8     : 8n1 (8bit,no parity,1 stopbit)
      CLOCAL  : local connection, no modem contol
      CREAD   : enable receiving characters
      CSTOP   : two stop bits, else one
    */

    serial_.c_cflag = (B9600 | CS8 | CLOCAL | CREAD | CSTOPB | ~PARENB);   // TWO stop bits
    serial_.c_iflag = IGNPAR | INPCK;
    serial_.c_oflag = 0;
    serial_.c_lflag = 0;
//    serial_.c_lflag = ~ISIG | ICANON;        // Disable signals

    serial_.c_cc[VMIN] = 0;
    serial_.c_cc [VTIME] = 50 ; // Five seconds (TIME * 0.1s)

    tcflush(fd_, TCIOFLUSH);
    tcsetattr(fd_, TCSANOW, &serial_);

    return 0;
}

int Antenna::sendData(char* data, size_t length)
{

    int wcount = write(fd_, data, length);

    if (wcount < 0) {
        perror("Write");
        return -1;
    }

    return wcount;
}

int Antenna::recData(char* data, size_t length)
{
    int rcount = read(fd_, data, length);
    if (rcount < 0)
    {
        perror("Read");
        return -1;
    }

    return rcount;
}
