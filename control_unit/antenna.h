#ifndef ANTENNA_H
#define ANTENNA_H

#include <unistd.h>			//Used for UART
#include <fcntl.h>			//Used for UART
#include <stdlib.h>
#include <termios.h>
#include <cstring>
#include <cstdio>

class Antenna
{
public:
    Antenna();
    ~Antenna();
    int sendData(char* data, size_t length);
    int recData(char* data, size_t length);

protected:
    struct termios serial_, old_serial_;
    int fd_;

    int initUart();
};

#endif // ANTENNA_H
