#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QFuture>
#include <QtConcurrent/QtConcurrentRun>

#include "player.h"
#include "adcserial.h"
#include "antenna.h"
#include "calibratedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    AdcSerial* adc_;
    Antenna* uart_;
    QMutex mutex_;
    struct player pl_[3];
    bool raceStarted_;

    bool isReady(int channel);
    void raceThread();
    static int normalize(int value, controller &c);

private slots:
    void bindChannel();
    void calibrateController();
    void startRace();
    void stopRace();
    void chMode(int index);

};

#endif // MAINWINDOW_H
