/*
 * Car_Mode_FT.c
 *
 * Created: 09.04.2017 23:44:57
 * Author : Dmytro
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/sfr_defs.h>
#include <stdbool.h>
#include <avr/interrupt.h>

struct engine
{
	int MaxVal, MidVal, MinVal;
	int speed;
	int engType;					// 1 - GAS Engine; 2 - CONTROL Engine (Servo).
};

struct engine en[2] = {	{455, 380, 160, 0, 1},			//	gas		---before 420, 380, 160---
						{465, 375, 300, 0, 2} };		//	control

int ubrr = 103;		// For BAUD RATE 9600
const unsigned long tickMax = 62;
unsigned long tickCount;
char cmdBuff[25];
char valueStr[5];
unsigned int iter = 0;
char carChannel;
bool raceState = false;
bool pastButtonState = false;
bool currButtonState = false;
bool waitBinding = false;
bool readyBind = false;
bool startSymbol = false;
bool signalLost = false;
bool speedMode = false;							// KID - false, NORMAL - true


void InitUART()
{
	/* Set BAUD RATE, RX, TX and RXComplete Interrupt  */
	UBRRH = (unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;
	UCSRB = _BV(RXEN)|_BV(TXEN)|_BV(RXCIE);
	/* Set frame 8 data bits , 2 stop bits */
	UCSRC = _BV(URSEL)|_BV(USBS)|_BV(UCSZ1)|_BV(UCSZ0);
}

void InitPWM()
{
	DDRB |= _BV(DDB2)|_BV(DDB1);
	// PB1 and PB2 are now outputs
	//  TCCR1B &= ~(_BV(CS10)|_BV(CS11)|_BV(CS12));  
	//   Stop Timer/Counter1
	TCNT1 = 0;									
	TCCR1A = 0;
	TCCR1B = 0;
	// clear registers
	TCCR1A |= _BV(COM1B1)|_BV(COM1A1);
	// set none-inverting mode
	TCCR1A |= _BV(WGM11);
	TCCR1B |= _BV(WGM13) | _BV(WGM12);
	// set fast PWM Mode
	TCCR1B |= _BV(CS11)| _BV(CS10);
	// set prescaler 64 and start pwm
	
	ICR1 = 2500;
	OCR1A = 380;								// acceleration
	OCR1B = 375;								// control
	// fill TOP and compare registers
	
	TIMSK &=~(_BV(TOIE1)|_BV(OCIE1B)|_BV(OCIE1A)|_BV(TICIE1)|_BV(TOIE2)|_BV(OCIE2)) ;
	// disable all interrupts by timer1 and timer2
}

void InitTimer()
{
	TCNT0 = 0;
	TCCR0 &= ~(_BV(CS01)|_BV(CS00));	/* Prescaler 256 */
	TCCR0 |= _BV(CS02);
}

void EnginePower(bool stat)
{
	if(stat)
	PORTC |= _BV(PORTC5);		/* Turn on engine power  */
	else
	PORTC &= ~_BV(PORTC5);		/* Turn off engine power  */
}

void CameraPower(bool stat)
{
	if(stat)
	PORTC |= _BV(PORTC4);		/* Turn on camera power  */
	else
	PORTC &= ~_BV(PORTC4);		/* Turn off camera power  */
}

void ShiftReg()
{
	PORTB |= _BV(DDB4);
	PORTB &= ~_BV(DDB4);
}

void InputOne()
{
	PORTB |= _BV(DDB3);
	ShiftReg();
	PORTB &= ~_BV(DDB3);
}

void InputZero()
{
	PORTB &= ~_BV(DDB3);
	ShiftReg();
}

bool Debounce(bool last)
{
	bool current = !(PIND & _BV(DDD7));
	if (last != current)
	{
		_delay_ms(5);
		current = !(PIND & _BV(DDD7));
	}
	return current;
}

void NormalizeValue(struct engine * e, int value)
{
	int res = e->MidVal;
	value -= 100;
	if (value != 0)
	{
		if (value > 0)
		{
			if((!speedMode) && (e->engType == 1))
			{
				res = e->MidVal + ((20 * value) / 100);					// --before: 15 - 20% of Max--
			}
			else
			{
				res = e->MidVal + (((e->MaxVal - e->MidVal) * value) / 100);
			}
			if (res > e->MaxVal)
			{
				res = e->MaxVal;
			}
		}
		else
		{
			if((!speedMode) && (e->engType == 1))
			{
				res = e->MidVal + ((100 * value) / 100);
			}
			else
			{
				res = e->MinVal + (((e->MidVal - e->MinVal) * (100 + value)) / 100);
			}
			if (res < e->MinVal)
			{
				res = e->MinVal;
			}
		}
	}
	
	e->speed = res;
}

void SetStateTimer0(bool state)
{
	if(state)
	{
		TCNT0 = 0;
		tickCount = tickMax;
		TIMSK |= _BV(TOIE0);
	}
	else
	{
		TIMSK &= ~_BV(TOIE0);
	}
}

void USART_Transmit(unsigned char data)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSRA & _BV(UDRE)) )
	;
	/* Put data into buffer, sends the data */
	UDR = data;
}

void USART_SendStr(char *s)		//  Send constant string
{
	while (*s != 0) USART_Transmit(*s++);
}

ISR(USART_RXC_vect)
{
	char ch_temp = UDR;
	if (ch_temp == '#')
	{
		startSymbol = true;
		iter = 0;
	}
	else if(startSymbol)
	{
		cmdBuff[iter] = ch_temp;
		iter++;
		if (ch_temp == '?')
		{
			if(cmdBuff[0] == 'B' && waitBinding)
			{
				carChannel = cmdBuff[1];		// Remember number of channel
				InputOne();						// Turn off BLUE light
				InputZero();
				waitBinding = false;
				readyBind = true;
				USART_SendStr("Bind+");
			}
			else if((cmdBuff[0] == 'C') && (cmdBuff[1] == carChannel) && readyBind)
			{
				if (cmdBuff[2] == 'M')
				{
					if(cmdBuff[3] == 'N') { speedMode = true;}
					else { speedMode = false;}
				}
				else if (cmdBuff[2] == 'T')
				{
					if(cmdBuff[3] == '-')
					{
						en[1].MidVal--;
					}
					else if(cmdBuff[3] == '+')
					{
						en[1].MidVal++;
					}
					OCR1B = en[1].MidVal;
				}
				else if ((cmdBuff[2] == 'R') && raceState)
				{
					unsigned int tmp_iter = 3;		// Next symbol after 'R'
					unsigned int i = 0;
					while ((cmdBuff[tmp_iter] != '/') && (cmdBuff[tmp_iter] >= '0') && (cmdBuff[tmp_iter] <= '9'))
					{
						valueStr[i] = cmdBuff[tmp_iter];
						i++;
						tmp_iter++;
					}
					NormalizeValue(&en[0], atoi(valueStr));
					memset(valueStr, '\0', 5);
					tmp_iter++;
					i = 0;
					while ((cmdBuff[tmp_iter] != '?') && (cmdBuff[tmp_iter] >= '0') && (cmdBuff[tmp_iter] <= '9'))
					{
						valueStr[i] = cmdBuff[tmp_iter];
						i++;
						tmp_iter++;
					}
					NormalizeValue(&en[1], atoi(valueStr));
					OCR1A = en[0].speed;
					OCR1B = en[1].speed;
					memset(valueStr, '\0', 5);
					SetStateTimer0(true);
					if (signalLost)								// If connection was lost but now restored
					{
						InputOne();									// Turn off BLUE light
						InputZero();
						signalLost = false;
					}
				}
				
			}
			else if ((strncmp(cmdBuff, "Start?", 6) == 0) && readyBind)
			{
				raceState = true;
				EnginePower(true);
				CameraPower(true);
			}
			else if (strncmp(cmdBuff, "Stop?", 5) == 0)
			{
				raceState = false;
				EnginePower(false);
				CameraPower(false);
				OCR1A = en[0].MidVal;
				OCR1B = en[1].MidVal;
				SetStateTimer0(false);
				InputOne();									// Turn off BLUE light
				InputZero();
				signalLost = false;
			}
			iter = 0;
			startSymbol = false;
		}
	}
}


ISR (TIMER0_OVF_vect)
{
	if (tickCount <= 0)
	{
		TCNT0 = 0;
		tickCount = tickMax;
		if(raceState)
		{
			OCR1A = en[0].MidVal;
			OCR1B = en[1].MidVal;
			InputOne();									// Set BLUE light too
			InputOne();
			signalLost = true;
		}
	}
	else
	{
		tickCount--;
	}
}



int main(void)
{
	cli();
	DDRB |= _BV(DDB4)|_BV(DDB3);						// Enable Shift Register pins
	DDRD &= ~_BV(DDD7);									// Set BUTTON pin as input
	PORTD |= _BV(DDD7);									// Set BUTTON pull-up
	DDRC |= _BV(DDC5)|_BV(DDC4);						// Set to output Engine and Camera Relay pins
	InitUART();
	InitPWM();
	InitTimer();
	
	_delay_ms(5);										// Time to set up configuration
	
	InputOne();											// Set GREEN light
	ShiftReg();
	
	sei();

	while (1)
	{
		currButtonState = Debounce(pastButtonState);
		if((pastButtonState == false) && (currButtonState == true) && (waitBinding == false))		// if the BUTTON have been pushed
		{
			InputOne();									// Set BLUE light too
			InputOne();
			waitBinding = true;
		}
		else if ((pastButtonState == false) && (currButtonState == true) && (waitBinding == true))
		{
			InputOne();									// Turn off BLUE light
			InputZero();
			waitBinding = false;
		}
		pastButtonState = currButtonState;
	}
}